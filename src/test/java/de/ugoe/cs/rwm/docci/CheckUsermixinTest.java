package de.ugoe.cs.rwm.docci;

import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.Mixin;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.connector.LocalhostConnector;

public class CheckUsermixinTest {
	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestUtility.extensionRegistrySetup();
	}

	@Test
	public void getUnregisteredMixins() {

		Logger.getRootLogger().setLevel(Level.FATAL);

		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/WaaSArchitecture.occic"));

		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);
		Resource targetModel = ModelUtility.loadOCCIintoEMFResource(occiPath);
		deployer.getUnregisteredMixins(targetModel);

		assertTrue(true);
	}

	@Test
	public void registerUnregisteredMixins() {

		Logger.getRootLogger().setLevel(Level.FATAL);

		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/WaaSArchitecture.occic"));

		// Transformator trans = TransformatorFactory.getTransformator("OCCIC2OCCIC");
		// trans.transform(occiPath, occiPath);
		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);
		Resource targetModel = ModelUtility.loadOCCIintoEMFResource(occiPath);
		EList<Mixin> unregisteredMixins = deployer.getUnregisteredMixins(targetModel);
		deployer.registerMixins(unregisteredMixins);
		unregisteredMixins = deployer.getUnregisteredMixins(targetModel);

		assertTrue(true);
	}
}
