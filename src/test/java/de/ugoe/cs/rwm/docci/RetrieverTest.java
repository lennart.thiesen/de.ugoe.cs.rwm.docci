package de.ugoe.cs.rwm.docci;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.Mixin;
import org.eclipse.cmf.occi.core.Resource;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.docci.connector.LocalhostConnector;

public class RetrieverTest {
	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestUtility.extensionRegistrySetup();
	}

	@Test
	public void retrieveRuntimeModel() {
		LocalhostConnector conn = new LocalhostConnector("localhost", 8080, "erbel");
		new File(System.getProperty("user.home") + "/.rwm/").mkdirs();
		Path test = conn.loadRuntimeModel(Paths.get(System.getProperty("user.home") + "/.rwm/runtime.occic"));

		org.eclipse.emf.ecore.resource.Resource testRes = ModelUtility.loadOCCIintoEMFResource(test);

		Configuration runtimeModel = (Configuration) testRes.getContents().get(0);

		System.out.println(runtimeModel);
		System.out.println(runtimeModel.getResources());
		for (Resource res : runtimeModel.getResources()) {
			System.out.print("Resource: " + res.getTitle());
			System.out.print(" : ");
			System.out.println(res.getKind().getTitle());
			for (Mixin mix : res.getMixins()) {
				System.out.println("     Mixin: " + mix.getTerm());
			}
			for (Link link : res.getLinks()) {
				System.out.println("     Link: " + link.getTitle());
			}
		}
	}
}
