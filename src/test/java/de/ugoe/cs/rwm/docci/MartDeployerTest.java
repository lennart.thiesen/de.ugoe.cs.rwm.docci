package de.ugoe.cs.rwm.docci;

import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.connector.LocalhostConnector;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.TransformatorFactory;
import de.ugoe.cs.rwm.tocci.occi2openstack.OCCI2OPENSTACKTransformator;

public class MartDeployerTest {
	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestUtility.extensionRegistrySetup();
	}

	@Before
	public void deprovisionEverything() {
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/Empty.occic"));
		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);

		deployer.deploy(occiPath);
	}

	@After
	public void clearResourceCache() {
		CachedResourceSet.getCache().clear();
	}

	@Test
	public void deployInfra() {

		Logger.getRootLogger().setLevel(Level.FATAL);

		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/Infra.occic"));

		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);
		deployer.deploy(occiPath);

		assertTrue(TestUtility.equalsRuntime(occiPath, conn));
	}

	@Test
	public void deployPlatform() {

		Logger.getRootLogger().setLevel(Level.FATAL);

		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/PaaS.occic"));

		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);

		deployer.deploy(occiPath);

		assertTrue(TestUtility.equalsRuntime(occiPath, conn));
	}

	@Test
	public void deployWaaSArchitecture() throws EolRuntimeException {

		Logger.getRootLogger().setLevel(Level.FATAL);

		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/WaaSArchitecture.occic"));

		Transformator trans = TransformatorFactory.getTransformator("OCCI2OCCI");
		trans.transform(occiPath, occiPath);
		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);

		deployer.deploy(occiPath);

		assertTrue(TestUtility.equalsRuntime(occiPath, conn));
	}

	@Test
	public void deployHCluster() throws EolRuntimeException {
		Logger.getRootLogger().setLevel(Level.FATAL);

		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/hadoopCluster.occic"));

		Transformator trans = TransformatorFactory.getTransformator("OCCI2OCCI");
		trans.transform(occiPath, occiPath);
		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);

		deployer.deploy(occiPath);

		assertTrue(TestUtility.equalsRuntime(occiPath, conn));
	}

	@Test
	public void bionicBeaver() throws EolRuntimeException {
		Logger.getRootLogger().setLevel(Level.FATAL);

		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/MLSProviderManagementNWU18.occic"));

		Transformator trans = TransformatorFactory.getTransformator("OCCI2OCCI");
		trans.transform(occiPath, occiPath);
		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);

		deployer.deploy(occiPath);

		assertTrue(TestUtility.equalsRuntime(occiPath, conn));
	}

	@Test
	public void deployAttachedComponent() throws EolRuntimeException {
		Logger.getRootLogger().setLevel(Level.FATAL);

		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/hadoop.occic"));

		Transformator trans = TransformatorFactory.getTransformator("OCCI2OCCI");
		trans.transform(occiPath, occiPath);

		OCCI2OPENSTACKTransformator trans2 = new OCCI2OPENSTACKTransformator();
		trans2.setTransformationProperties("718eaf4e-cc2b-4f19-9347-8ba8a045c515", "", "",
				"urn:uuid:29d78078-fb4c-47aa-a9af-b8aaf3339590");
		trans2.transform(occiPath, occiPath);

		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);

		deployer.deploy(occiPath);

		Path occiPath2 = Paths.get(ModelUtility.getPathToResource("occi/Spark.occic"));
		trans.transform(occiPath2, occiPath2);
		trans2.transform(occiPath2, occiPath2);
		deployer.deploy(occiPath2);

		assertTrue(TestUtility.equalsRuntime(occiPath2, conn));
	}
}
