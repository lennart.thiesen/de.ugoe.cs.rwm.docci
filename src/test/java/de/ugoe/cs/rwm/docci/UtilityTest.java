package de.ugoe.cs.rwm.docci;

import static org.junit.Assert.assertFalse;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.cmf.occi.core.Attribute;
import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.cmf.occi.core.Mixin;
import org.eclipse.cmf.occi.core.Resource;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.junit.BeforeClass;
import org.junit.Test;

public class UtilityTest {

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestUtility.extensionRegistrySetup();
	}

	@Test
	public void resolveProxiesMLS_Hadoop() {
		StringBuilder toCheck = new StringBuilder();
		CachedResourceSet.getCache().clear();
		Path newOCCI = Paths.get(ModelUtility.getPathToResource("occi/MLS_Hadoop.occic"));
		EList<EObject> newModel = ModelUtility.loadOCCI(newOCCI);
		for (Resource res : ModelUtility.getResources(newModel)) {
			toCheck.append(res.getKind());
			toCheck.append(res.getKind().getTerm());

			for (Attribute attr : res.getKind().getAttributes()) {
				toCheck.append(attr);
			}

			for (Mixin mix : res.getMixins()) {
				toCheck.append(mix);
				toCheck.append(mix.getScheme());
				toCheck.append(mix.getTerm());
			}
		}
		System.out.println(toCheck.toString());
		assertFalse(toCheck.toString().contains("eProxy"));
	}

	@Test
	public void resolveProxiesMLS_DiffVM() {
		StringBuilder toCheck = new StringBuilder();
		CachedResourceSet.getCache().clear();
		Path newOCCI = Paths.get(ModelUtility.getPathToResource("occi/MLSDiffVM.occic"));
		EList<EObject> newModel = ModelUtility.loadOCCI(newOCCI);
		for (Resource res : ModelUtility.getResources(newModel)) {
			toCheck.append(res.getKind());
			toCheck.append(res.getKind().getTerm());

			for (Attribute attr : res.getKind().getAttributes()) {
				toCheck.append(attr);
			}

			for (Mixin mix : res.getMixins()) {
				toCheck.append(mix);
				toCheck.append(mix.getScheme());
				toCheck.append(mix.getTerm());
			}
		}
		System.out.println(toCheck.toString());
		assertFalse(toCheck.toString().contains("eProxy"));
	}

	@Test
	public void loadIntoResource() {
		// OcciRegistry.getInstance().initialize();
		CachedResourceSet.getCache().clear();
		Path newOCCI = Paths.get(ModelUtility.getPathToResource("occi/hadoopCluster.occic"));
		org.eclipse.emf.ecore.resource.Resource newModel = ModelUtility.loadOCCIintoEMFResource(newOCCI);
		Configuration config = (Configuration) newModel.getContents().get(0);
		for (Resource res : config.getResources()) {
			System.out.println(res.getKind());
			/*
			 * for(Attribute attr: res.getKind().getAttributes()) {
			 * System.out.println(attr); }
			 */
			for (Mixin mix : res.getMixins()) {
				System.out.println(mix);
				System.out.println(mix.getScheme());
			}
		}
	}

	@Test
	public void loadIntoResourceMLSHadoop() {
		// OcciRegistry.getInstance().initialize();
		CachedResourceSet.getCache().clear();
		Path newOCCI = Paths.get(ModelUtility.getPathToResource("occi/MLS_Hadoop.occic"));
		org.eclipse.emf.ecore.resource.Resource newModel = ModelUtility.loadOCCIintoEMFResource(newOCCI);
		System.out.println("TEST");
		System.out.println(newModel.getContents());
		for (EObject obj : newModel.getContents()) {
			System.out.println("TEST");
			if (obj instanceof Configuration) {
				Configuration config = (Configuration) obj;
				for (Resource res : config.getResources()) {
					System.out.println(res.getKind());
					/*
					 * for(Attribute attr: res.getKind().getAttributes()) {
					 * System.out.println(attr); }
					 */
					for (Mixin mix : res.getMixins()) {
						System.out.println(mix);
						System.out.println(mix.getScheme());
					}
				}
			}
		}

	}

	@Test
	public void loadConfiguration() {
		CachedResourceSet.getCache().clear();
		Path newOCCI = Paths.get(ModelUtility.getPathToResource("occi/MLS_Hadoop.occic"));
		Configuration newModel = ModelUtility.loadOCCIConfiguration(newOCCI);
		for (Resource res : newModel.getResources()) {
			System.out.println(res.getKind());
			/*
			 * for(Attribute attr: res.getKind().getAttributes()) {
			 * System.out.println(attr); }
			 */
			for (Mixin mix : res.getMixins()) {
				System.out.println(mix);
				System.out.println(mix.getScheme());
			}
		}
	}

}
