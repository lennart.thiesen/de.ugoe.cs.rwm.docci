package de.ugoe.cs.rwm.docci.containerrunner;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.Resource;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;

public class MartContainerRunnerMaster {
	static Logger LOGGER = Logger.getLogger(MartContainerRunnerMaster.class);
	private Connector conn;
	private volatile List<Resource> blockedVMs = new ArrayList<Resource>();
	private String jhs;

	public MartContainerRunnerMaster(Connector conn) {
		this.conn = conn;
	}

	public void runContainers(EList<Resource> container) {
		while (container.isEmpty() == false) {
			EList<Resource> appCycle = new BasicEList<Resource>();
			for (Resource res : container) {
				if (computeIsAlreadySourcedByContainer(res) == false) {
					appCycle.add(res);
					blockedVMs.addAll(getComputeNodesConnectedToContainer(res));
				}
			}
			List<MartContainerRunnerSlave> slaves = new ArrayList<MartContainerRunnerSlave>();
			List<Thread> threads = new ArrayList<Thread>();
			slaves.addAll(createSubtasks(appCycle));
			container.removeAll(appCycle);

			for (MartContainerRunnerSlave slave : slaves) {
				Thread thread = new Thread(slave);
				threads.add(thread);
				thread.start();
			}
			for (Thread t : threads) {
				LOGGER.info("Joining THreads");
				try {
					t.join();
					LOGGER.info("Thread: " + t + "joined");
				} catch (InterruptedException e) { // TODO Auto-generated catch
					e.printStackTrace();
				}
			}
			LOGGER.info("Threads joined: Emptying blocked list");
			blockedVMs.clear();
		}
	}

	private boolean computeIsAlreadySourcedByContainer(Resource res) {
		List<Resource> machineSources = getComputeNodesConnectedToContainer(res);
		for (Resource machine : machineSources) {
			if (blockedVMs.contains(machine)) {
				return true;
			}
		}
		return false;
	}

	private List<Resource> getComputeNodesConnectedToContainer(Resource container) {
		List<Resource> machines = new ArrayList<Resource>();
		for (Link pLink : container.getRlinks()) {
			if (ModelUtility.isMachine(pLink.getSource())) {
				machines.add(pLink.getTarget());
			}
		}
		return machines;
	}

	private List<MartContainerRunnerSlave> createSubtasks(EList<Resource> apps) {
		List<MartContainerRunnerSlave> slaves = new ArrayList<MartContainerRunnerSlave>();

		for (Resource res : apps) {
			MartContainerRunnerSlave slave = new MartContainerRunnerSlave(conn, res, jhs);
			slaves.add(slave);
		}
		return slaves;
	}

	public void setJobHistoryPath(String jobhistorypath) {
		this.jhs = jobhistorypath;
	}
}
