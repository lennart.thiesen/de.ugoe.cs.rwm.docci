/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci.executor;

import de.ugoe.cs.rwm.docci.connector.Connector;

/**
 * Factory class for executor objects.
 *
 * @author erbel
 *
 */
public class ExecutorFactory {
	/**
	 * Returns Executor instance.
	 *
	 * @param criteria
	 *            Accepts: "Mart".
	 * @return Executor instance.
	 */
	public static Executor getExecutor(String criteria, Connector conn) {
		if (criteria.equals("Mart")) {
			return new MartExecutor(conn);
		}
		return null;
	}
}
