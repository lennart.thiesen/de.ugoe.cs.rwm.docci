/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci.retriever;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

/**
 * Responsible for retrieving the runtime model from a separate host.
 *
 * @author erbel
 *
 */
public class ModelRetriever {
	static Logger LOG = Logger.getLogger(ModelRetriever.class.getName());

	/**
	 * Refreshes Mart runtime model, Opens an Sftp channel, and downloads the
	 * runtime model to the specified location.
	 *
	 * @param rdirectory
	 *            Remote directory of the runtime model.
	 * @param rfile
	 *            Runtime model file.
	 * @param lfile
	 *            Local file in which the runtime model will be stored.
	 * @param host
	 *            Address to the host containing the runtime model.
	 * @param port
	 *            Port of the host. (8080 for MART server)
	 * @param username
	 *            Username of the host.
	 * @param pKey
	 *            SSH key required to connect to the host.
	 */
	public static void retrieveRuntimeModel(String rdirectory, String rfile, String lfile, String host, int port,
			String username, String pKey) {
		refreshMartRuntimeModel(host, port);
		ChannelSftp sftp = connect(host, username, pKey);
		download(rdirectory, rfile, lfile, sftp);
		sftp.exit();
		try {
			sftp.getSession().disconnect();
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Refreshes the runtime model over the REST API of the MART Server.
	 *
	 * @param host
	 *            Address of the MART Server API.
	 * @param port
	 *            Port of the MART Server API.
	 */
	public static void refreshMartRuntimeModel(String host, int port) {
		URL url;
		HttpURLConnection conn = null;
		try {
			url = new URL("http://" + host + ":" + port + "/mart/save/");
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");

			//We skip 500 as the error appears regularly in the MartServer but does not have any influence on the
			//retrieved model
			if (conn.getResponseCode() != 200 && conn.getResponseCode() != 500 ) {
				LOG.info("Runtime Model could not be refreshed" + conn.getResponseCode());
				LOG.error("Error: " + conn.getResponseMessage());
			} else {
				LOG.info("Refreshed Runtime Model");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(conn != null) {
				conn.disconnect();
			}
		}
	}

	private static ChannelSftp connect(String host, String username, String privateKey) {
		ChannelSftp sftp = null;
		try {
			JSch jsch = new JSch();
			jsch.addIdentity(privateKey);
			Session sshSession = jsch.getSession(username, host);
			LOG.info("SFTP Session created. ");
			Properties sshConfig = new Properties();
			sshConfig.put("StrictHostKeyChecking", "no");
			sshSession.setConfig(sshConfig);
			sshSession.connect();
			LOG.debug("SFTP Session connected.");
			LOG.debug("SFTP Opening Channel.");
			Channel channel = sshSession.openChannel("sftp");
			channel.connect();
			sftp = (ChannelSftp) channel;
			LOG.info("SFTP Connected to " + host + ".");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sftp;
	}

	private static void download(String directory, String downloadFile, String saveFile, ChannelSftp sftp) {
		FileOutputStream fileOutputStream = null;
		try {
			sftp.cd(directory);
			File file = new File(saveFile);
			LOG.info("Storing Runtime Model at: " + file.getAbsolutePath());
			fileOutputStream = new FileOutputStream(file);
			sftp.get(downloadFile, fileOutputStream);
			LOG.debug("Downloaded File to: " + file.getAbsolutePath());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch (IOException e) {
					LOG.debug("Could not close file output stream");
					e.printStackTrace();
				}
			}
		}
	}
}