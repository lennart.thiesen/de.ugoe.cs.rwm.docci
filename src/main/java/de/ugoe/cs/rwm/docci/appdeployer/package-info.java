/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/
/**
 *
 */
/**
 * Pacakge containing the classes responsible for the deployment of
 * applications.
 * 
 * @author erbel
 *
 */
package de.ugoe.cs.rwm.docci.appdeployer;