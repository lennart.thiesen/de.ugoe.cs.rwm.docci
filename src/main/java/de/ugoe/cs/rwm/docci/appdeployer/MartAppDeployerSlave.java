/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci.appdeployer;

import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.Action;
import org.eclipse.cmf.occi.core.Resource;

import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.executor.Executor;
import de.ugoe.cs.rwm.docci.executor.ExecutorFactory;
import de.ugoe.cs.rwm.docci.history.DeplHistory;

/**
 * Slave objects posting the requests to bring an application into an active
 * state.
 *
 * @author erbel
 *
 */
public class MartAppDeployerSlave implements Runnable {
	static Logger LOGGER = Logger.getLogger(MartAppDeployerSlave.class);
	private Resource app;
	private Connector conn;
	private String jobhistorypath;

	/**
	 * Constructor for a MartAppDeployerSlave.
	 * 
	 * @param conn Connector to the OCCI API.
	 * @param app  Application to be deployed.
	 * @param jhs
	 */
	public MartAppDeployerSlave(Connector conn, Resource app, String jhs) {
		this.conn = conn;
		this.app = app;
		this.jobhistorypath = jhs;
	}

	@Override
	public void run() {
		this.deployApplication();
	}

	/**
	 * Method performing a deploy, configure and start request on the Objects app.
	 *
	 */
	private void deployApplication() {
		Executor exec = ExecutorFactory.getExecutor("Mart", conn);
		Action deploy = ModelUtility.getAction(app, "deploy");

		long deplStart = System.currentTimeMillis();

		LOGGER.info("Deploying: " + app.getTitle());
		exec.executeOperation("POST", app, deploy);

		long deplEnd = System.currentTimeMillis();

		long confStart = System.currentTimeMillis();

		Action configure = ModelUtility.getAction(app, "configure");
		exec.executeOperation("POST", app, configure);

		long confEnd = System.currentTimeMillis();

		long startStart = System.currentTimeMillis();

		Action start = ModelUtility.getAction(app, "start");
		exec.executeOperation("POST", app, start);

		long startEnd = System.currentTimeMillis();

		if (jobhistorypath != null) {
			DeplHistory hist = new DeplHistory(app, deplStart, deplEnd, confStart, confEnd, startStart, startEnd);
			hist.store(jobhistorypath + "/depl");
		}
	}
}
