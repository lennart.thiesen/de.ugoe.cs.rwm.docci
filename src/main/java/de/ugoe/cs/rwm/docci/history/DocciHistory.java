package de.ugoe.cs.rwm.docci.history;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Files;

import de.ugoe.cs.rwm.docci.Deployer;

public class DocciHistory extends JobHistory {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<DeplHistory> deplHistory;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<EntHistory> provHistory;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<EntHistory> deprovHistory;
	private Date endDeprovDate;
	private Date endProvDate;
	private long provDuration;
	private long deplDuration;
	private long deprovDuration;

	public DocciHistory() {
	}

	public DocciHistory(String jobName, String jobId, Date startDate, Date endProvDate, Date endDate, Deployer depl,
			String status) {
		this.readableStartDate = (Date) startDate.clone();
		this.startDate = startDate.getTime();

		this.readableEndDate = (Date) endDate.clone();
		this.endDate = endDate.getTime();

		// Assign start to end date to resolve jobs without deprovisioning at all
		this.endDeprovDate = (Date) startDate.clone();
		this.endProvDate = (Date) endProvDate.clone();
		this.jobId = jobId;
		this.duration = calculateDuration(endDate, startDate, TimeUnit.MILLISECONDS);

		this.readableDuration = formatToReadableDuration(duration);

		// Calculate prov date in case no deprov process was required
		this.provDuration = calculateDuration(endProvDate, startDate, TimeUnit.MILLISECONDS);
		this.deplDuration = calculateDuration(endDate, endProvDate, TimeUnit.MILLISECONDS);

		this.jobName = jobName;
		this.mode = depl.getClass().getSimpleName();
		this.book = "DOCCI";
		this.status = status;
		this.deplHistory = new ArrayList<>();
		this.provHistory = new ArrayList<>();
		this.deprovHistory = new ArrayList<>();
	}

	private void deriveDeplHistory(String jhs) {
		try {
			File[] fl = new File(jhs + "/depl").listFiles();
			if (fl != null) {
				for (final File fileEntry : fl) {
					if (Files.getFileExtension(fileEntry.getName()).equals("json")) {
						ObjectMapper objectMapper = new ObjectMapper();
						DeplHistory dHist = objectMapper.readValue(fileEntry, DeplHistory.class);
						deplHistory.add(dHist);
					}
				}
			}
		} catch (IOException e) {
			System.out.println("IOException: Could not create depl history!");
		}
	}

	private void deriveProvHistory(String jhs) {
		try {
			File[] fl = new File(jhs + "/prov").listFiles();
			if (fl != null) {
				for (final File fileEntry : fl) {
					if (Files.getFileExtension(fileEntry.getName()).equals("json")) {
						ObjectMapper objectMapper = new ObjectMapper();
						EntHistory pHist = objectMapper.readValue(fileEntry, EntHistory.class);
						provHistory.add(pHist);
					}
				}
			}
		} catch (IOException e) {
			System.out.println("IOException: Could not create prov history!");
		}
	}

	private void deriveDeprovHistory(String jhs) {
		try {
			File[] fl = new File(jhs + "/deprov").listFiles();
			if (fl != null) {
				for (final File fileEntry : fl) {
					if (Files.getFileExtension(fileEntry.getName()).equals("json")) {
						ObjectMapper objectMapper = new ObjectMapper();
						EntHistory pHist = objectMapper.readValue(fileEntry, EntHistory.class);
						deprovHistory.add(pHist);
					}
				}
				if (fl.length > 0) {
					deprovHistory.sort(Comparator.comparing(EntHistory::getStartDate));
					deprovDuration = calcDurationInMS(deprovHistory.get(deprovHistory.size() - 1).getEndDate(),
							deprovHistory.get(0).getStartDate());

					this.endDeprovDate = new Date(deprovHistory.get(deprovHistory.size() - 1).getEndDate());
					this.provDuration = provDuration - deprovDuration;
				}

			}
		} catch (IOException e) {
			System.out.println("IOException: Could not create prov history!");
		}
	}

	@Override
	public void storeHistory(String jobHistoryPath) {
		deriveDeplHistory(jobHistoryPath);
		deriveProvHistory(jobHistoryPath);
		deriveDeprovHistory(jobHistoryPath);
		try {
			convertToJson(jobHistoryPath);
		} catch (IOException e) {
			System.out.println("Could not store History.");
			e.printStackTrace();
		}
	}

	public List<DeplHistory> getDeplHistory() {
		return deplHistory;
	}

	public void setDeplHistory(List<DeplHistory> deplHistory) {
		this.deplHistory = deplHistory;
	}

	public List<EntHistory> getProvHistory() {
		return provHistory;
	}

	public void setProvHistory(List<EntHistory> provHistory) {
		this.provHistory = provHistory;
	}

	public Date getEndProvDate() {
		Date d = endProvDate;
		return d;
	}

	public void setEndProvDate(Date endProvDate) {
		this.endProvDate = (Date) endProvDate.clone();
	}

	public long getProvDuration() {
		return provDuration;
	}

	public void setProvDuration(long provDuration) {
		this.provDuration = provDuration;
	}

	public long getDeplDuration() {
		return deplDuration;
	}

	public void setDeplDuration(long deplDuration) {
		this.deplDuration = deplDuration;
	}

	public List<EntHistory> getDeprovHistory() {
		return deprovHistory;
	}

	public void setDeprovHistory(List<EntHistory> deprovHistory) {
		this.deprovHistory = deprovHistory;
	}

	public long getDeprovDuration() {
		return deprovDuration;
	}

	public void setDeprovDuration(long deprovDuration) {
		this.deprovDuration = deprovDuration;
	}

	public Date getEndDeprovDate() {
		Date d = endDeprovDate;
		return d;
	}

	public void setEndDeprovDate(Date endDeprovDate) {
		this.endDeprovDate = (Date) endDeprovDate.clone();
	}
}
