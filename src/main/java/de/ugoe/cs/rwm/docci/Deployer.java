/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci;

import java.nio.file.Path;

import org.eclipse.emf.ecore.resource.Resource;

/**
 * Deployer Interface. Entry point to trigger the deployment process for OCCI
 * models.
 *
 * @author erbel
 *
 */
public interface Deployer {
	/**
	 * Depploys the OCCI model specified in the targetConfigPath.
	 *
	 * @param targetConfigPath
	 *            Path to the OCCI model to be deployed.
	 */
	public void deploy(Path targetConfigPath);

	/**
	 * Depploys the OCCI model specified in the targetConfigResource.
	 *
	 * @param targetConfig
	 *            EMF Resource of the OCCI model to be deployed.
	 */
	public void deploy(Resource targetConfig);

	public void setJobHistoryPath(String jhp);

}
