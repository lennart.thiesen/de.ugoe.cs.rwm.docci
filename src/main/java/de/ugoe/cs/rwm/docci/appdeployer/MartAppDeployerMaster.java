/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci.appdeployer;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.Resource;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.modmacao.occi.platform.Application;
import org.modmacao.occi.platform.Component;
import org.modmacao.occi.platform.Componentlink;

import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;

/**
 * Master handling the creation requests to deploy Applications
 *
 * @author erbel
 *
 */
public class MartAppDeployerMaster {
	static Logger LOGGER = Logger.getLogger(MartAppDeployerMaster.class);
	private Connector conn;
	private volatile List<Resource> blockedVMs = new ArrayList<Resource>();
	private String jhs;

	/**
	 * MartAppDeployerMaster Constructor
	 *
	 * @param conn Connector to be used for the application deployment process.
	 */
	public MartAppDeployerMaster(Connector conn) {
		this.conn = conn;
	}

	/**
	 * Starts MartAppDeployer Slaves sending requests to deploy the appplications
	 * passed.
	 *
	 * @param apps List of applications to be deployed.
	 */
	public void deployApplications(EList<Resource> apps) {
		/*
		 * for (Resource app : apps) { MartAppDeployerSlave slave = new
		 * MartAppDeployerSlave(conn, app); slave.run(); }
		 */

		// EList<Resource> queuedApps = queueApps(apps);

		while (apps.isEmpty() == false) {
			EList<Resource> appCycle = new BasicEList<Resource>();
			for (Resource res : apps) {
				if (computeIsAlreadyTargetedByApp(res) == false) {
					appCycle.add(res);
					blockedVMs.addAll(getComputeNodesConnectedToApp(res));
				}
			}
			List<MartAppDeployerSlave> slaves = new ArrayList<MartAppDeployerSlave>();
			List<Thread> threads = new ArrayList<Thread>();
			slaves.addAll(createSubtasks(appCycle));
			apps.removeAll(appCycle);

			for (MartAppDeployerSlave slave : slaves) {
				Thread thread = new Thread(slave);
				threads.add(thread);
				thread.start();
			}
			for (Thread t : threads) {
				try {
					t.join();
				} catch (InterruptedException e) { // TODO Auto-generated catch
					e.printStackTrace();
				}
			}
			LOGGER.debug("Threads joined: Emptying blocked list");
			blockedVMs.clear();
		}

		/*
		 * List<MartAppDeployerSlave> slaves = new ArrayList<MartAppDeployerSlave>();
		 * List<Thread> threads = new ArrayList<Thread>();
		 * slaves.addAll(createSubtasks(apps)); LOGGER.debug("Slaves created: " +
		 * slaves.size());
		 *
		 * for (MartAppDeployerSlave slave : slaves) { Thread thread = new
		 * Thread(slave); threads.add(thread); thread.start(); } for (Thread t :
		 * threads) { LOGGER.info("Joining THreads"); try { t.join();
		 * LOGGER.info("Thread: " + t +"joined"); } catch (InterruptedException e) { //
		 * TODO Auto-generated catch e.printStackTrace(); } }
		 * LOGGER.info("Threads joined");
		 */
	}

	private boolean computeIsAlreadyTargetedByApp(Resource res) {
		List<Resource> computeTargets = getComputeNodesConnectedToApp(res);
		for (Resource compute : computeTargets) {
			if (blockedVMs.contains(compute)) {
				return true;
			}
		}
		return false;
	}

	private List<Resource> getComputeNodesConnectedToApp(Resource res) {
		List<Resource> comps = new ArrayList<Resource>();
		for (Link cLink : res.getLinks()) {
			if (ModelUtility.isComponent(cLink.getTarget())) {
				comps.addAll(getComputeNodesConnectedToComp(cLink.getTarget()));
			}
		}
		return comps;
	}

	private List<Resource> getComputeNodesConnectedToComp(Resource comp) {
		List<Resource> computes = new ArrayList<Resource>();
		for (Link pLink : comp.getLinks()) {
			if (ModelUtility.isCompute(pLink.getTarget())) {
				computes.add(pLink.getTarget());
			}
			if (ModelUtility.isComponent(pLink.getTarget())) {
				computes.addAll(getComputeNodesConnectedToComp(pLink.getTarget()));
			}
		}
		return computes;
	}

	/**
	 * Returns List of slaves responsible for sending the deployment requests.
	 *
	 * @param apps List of applications to be deployed.
	 * @return slaves List of Slave threads.
	 */
	private List<MartAppDeployerSlave> createSubtasks(EList<Resource> apps) {
		List<MartAppDeployerSlave> slaves = new ArrayList<MartAppDeployerSlave>();

		for (Resource res : apps) {
			MartAppDeployerSlave slave = new MartAppDeployerSlave(conn, res, jhs);
			slaves.add(slave);
		}
		return slaves;
	}

	public void setJobHistoryPath(String jobhistorypath) {
		this.jhs = jobhistorypath;
	}

	public void startAttachedComponents(org.eclipse.emf.ecore.resource.Resource runtimeModel) {
		for (org.eclipse.cmf.occi.core.Resource app : ModelUtility.getApplications(runtimeModel)) {
			if (((Application) app).getOcciAppState().getLiteral().equals("active")) {
				for (Link link : app.getLinks()) {
					if (link instanceof Componentlink) {
						Component comp = (Component) link.getTarget();
						if (comp.getOcciComponentState().getLiteral().equals("active") == false) {
							MartAppDeployerSlave slave = new MartAppDeployerSlave(conn, comp, jhs);
							slave.run();
						}
					}
				}
			}
		}
	}
}
