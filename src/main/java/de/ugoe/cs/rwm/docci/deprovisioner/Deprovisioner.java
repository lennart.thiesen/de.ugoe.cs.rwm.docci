/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci.deprovisioner;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.Action;
import org.eclipse.cmf.occi.core.Entity;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.Resource;
import org.eclipse.cmf.occi.infrastructure.Compute;
import org.eclipse.cmf.occi.infrastructure.Network;
import org.eclipse.cmf.occi.infrastructure.Storage;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.modmacao.occi.platform.Component;

import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.executor.Executor;
import de.ugoe.cs.rwm.docci.history.EntHistory;

/**
 * Handles the deprovisioning process.
 *
 * @author erbel
 *
 */
public class Deprovisioner {
	private static Logger log = Logger.getLogger(Deprovisioner.class.getName());
	private final String managementNWid;
	private Executor exec;
	private String jhs;

	/**
	 * Creates a deprovisioner instance for the specified Connector conn.
	 *
	 * @param executor      Executor to be used for the deprovisioning process.
	 * @param managemntNWid Id of the Management Network (does not get deleted).
	 */
	public Deprovisioner(Executor executor, String managemntNWid) {
		this.exec = executor;
		this.managementNWid = managemntNWid;
	}

	/**
	 * Deprovisions every element on the cloud infrastructure that is defined as
	 * EObject in the passed eList.
	 * 
	 * Special deletion mechanism for Containers: The contains links for containers
	 * have to be deleted after the container instances, because the connection to
	 * their docker host is required for the deletion process.
	 *
	 * @param eList List of Objects to be deprovisioned.
	 */
	public void deprovision(EList<EObject> eList) {
		EList<Entity> resources = new BasicEList<>();
		EList<Entity> networks = new BasicEList<>();
		EList<Entity> storages = new BasicEList<>();
		EList<Entity> computes = new BasicEList<>();
		EList<Entity> links = new BasicEList<>();
		EList<Entity> components = new BasicEList<>();
		EList<Entity> containers = new BasicEList<>();
		EList<Entity> hosts = new BasicEList<>();

		if (jhs != null) {
			jhs = jhs + "/deprov";
		}

		for (EObject element : eList) {
			if (element instanceof Resource) {
				Entity entity = (Entity) element;
				if (entity instanceof Compute) {
					if (entity.getKind().getTerm().toLowerCase().equals("container")) {
						containers.add(entity);
					} else {
						computes.add(entity);
					}
				} else if (entity instanceof Network) {
					if (!entity.getId().equals(managementNWid)) {
						networks.add(entity);
					}
				} else if (entity instanceof Storage) {
					storages.add(entity);
				} else if (entity instanceof Component) {
					components.add(entity);
				} else {
					resources.add(entity);
				}

			} else if (element instanceof Link) {
				Entity entity = (Entity) element;
				links.add(entity);
			}

			hosts.addAll(computes);
			hosts.addAll(containers);
		}

		long start;
		for (Entity component : components) {
			start = System.currentTimeMillis();
			undeployComponentInstance(component, hosts);
			if (jhs != null) {
				EntHistory hist = new EntHistory(component, start, System.currentTimeMillis());
				hist.store(jhs);
			}
		}

		for (Entity container : containers) {
			Resource res = (Resource) container;
			links.removeAll(res.getLinks());
			links.removeAll(res.getRlinks());
		}
		List<DeprovisionerThread> dts = createDTS(containers);
		List<Thread> threads = new ArrayList<Thread>();
		for (DeprovisionerThread t : dts) {
			Thread thread = new Thread(t);
			threads.add(thread);
			thread.start();
		}
		for (Thread t : threads) {
			try {
				t.join();
			} catch (InterruptedException e) { // TODO Auto-generated catch
				e.printStackTrace();
			}
		}

		for (Entity link : links) {
			start = System.currentTimeMillis();
			deprovisionLinkInstance(link);
			if (jhs != null) {
				EntHistory hist = new EntHistory(link, start, System.currentTimeMillis());
				hist.store(jhs);
			}
		}
		for (Entity component : components) {
			deprovisionComponentInstance(component, hosts);
		}

		for (Entity compute : computes) {
			start = System.currentTimeMillis();
			deprovisionComputeInstance(compute);
			if (jhs != null) {
				EntHistory hist = new EntHistory(compute, start, System.currentTimeMillis());
				hist.store(jhs);
			}
		}
		for (Entity storage : storages) {
			start = System.currentTimeMillis();
			deprovisionStorageInstance(storage);
			if (jhs != null) {
				EntHistory hist = new EntHistory(storage, start, System.currentTimeMillis());
				hist.store(jhs);
			}
		}
		for (Entity network : networks) {
			start = System.currentTimeMillis();
			deprovisionNetworkInstance(network);
			if (jhs != null) {
				EntHistory hist = new EntHistory(network, start, System.currentTimeMillis());
				hist.store(jhs);
			}
		}
		for (Entity resource : resources) {
			start = System.currentTimeMillis();
			deprovisionResourceInstance(resource);
			if (jhs != null) {
				EntHistory hist = new EntHistory(resource, start, System.currentTimeMillis());
				hist.store(jhs);
			}
		}
	}

	private List<DeprovisionerThread> createDTS(EList<Entity> entities) {
		List<DeprovisionerThread> dts = new ArrayList<>();
		for (Entity entity : entities) {
			dts.add(new DeprovisionerThread(entity, exec, jhs));
		}
		return dts;
	}

	/**
	 * Deprovisioning process for a single link instance.
	 *
	 * @param link Link to be deprovisioned.
	 */
	private void deprovisionLinkInstance(Entity link) {
		log.info("Deprovision Link: " + link.getKind());
		exec.executeOperation("DELETE", link, null);
	}

	/**
	 * Deprovisioning process for a single storage instance.
	 *
	 * @param entity Storage to be deprovisioned.
	 */
	private void deprovisionStorageInstance(Entity entity) {
		log.info("Deprovision Storage: " + entity.getTitle());
		exec.executeOperation("DELETE", entity, null);
	}

	/**
	 * Deprovisions a NetworkInstance.
	 *
	 * @param entity Network to be deprovisioned.
	 */
	private void deprovisionNetworkInstance(Entity entity) {
		log.info("Deprovision Network: " + entity.getTitle());
		exec.executeOperation("DELETE", entity, null);
	}

	/**
	 * Deprovisions a ComputeInstance
	 *
	 * @param entity Compute to be deprovisioned.
	 */
	private void deprovisionComputeInstance(Entity entity) {
		log.info("Deprovision Compute: " + entity.getTitle());
		exec.executeOperation("DELETE", entity, null);
	}

	/**
	 * Deprovisions a Component Instance
	 *
	 * @param entity   Component to be deprovisioned.
	 * @param computes List of Compute Nodes also deprovisioned in the process
	 */
	private void deprovisionComponentInstance(Entity entity, EList<Entity> computes) {
		if (isConnectedToCompute(entity) && hostingComputeNodeRemains(entity, computes)) {
			Action stop = ModelUtility.getAction(entity, "stop");
			Action undeploy = ModelUtility.getAction(entity, "undeploy");
			log.info("Hosting Compute Node Remains => Undeploying Component: " + entity.getTitle());
			exec.executeOperation("POST", entity, stop);
			exec.executeOperation("POST", entity, undeploy);
		}
		log.info("Deprovision Component: " + entity.getTitle());
		exec.executeOperation("DELETE", entity, null);
	}

	/**
	 * Deprovisions a Component Instance
	 *
	 * @param entity   Component to be deprovisioned.
	 * @param computes List of Compute Nodes also deprovisioned in the process
	 */
	private void undeployComponentInstance(Entity entity, EList<Entity> computes) {
		if (isConnectedToCompute(entity) && hostingComputeNodeRemains(entity, computes)) {
			Action stop = ModelUtility.getAction(entity, "stop");
			Action undeploy = ModelUtility.getAction(entity, "undeploy");
			log.info("Hosting Compute Node Remains => Undeploying Component: " + entity.getTitle());
			exec.executeOperation("POST", entity, stop);
			exec.executeOperation("POST", entity, undeploy);
		}
	}

	private boolean isConnectedToCompute(Entity entity) {
		Resource res = (Resource) entity;
		for (Link link : res.getLinks()) {
			if (link.getTarget() instanceof Compute) {
				return true;
			}
		}
		return false;
	}

	private boolean hostingComputeNodeRemains(Entity entity, EList<Entity> computes) {
		Resource res = (Resource) entity;
		for (Link link : res.getLinks()) {
			for (Entity ent : computes) {
				if (link.getTarget() != null) {
					if (ent.getId().equals(link.getTarget().getId())) {
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * Deprovisions a ResourceInstance
	 *
	 * @param entity Genereal Resource to be deprovisioned.
	 */
	private void deprovisionResourceInstance(Entity entity) {
		log.info("Deprovision Resource: " + entity.getTitle());
		exec.executeOperation("DELETE", entity, null);
	}

	public void setJhs(String jhs) {
		this.jhs = jhs;
	}
}
