/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci.connector;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;

import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.retriever.ModelRetriever;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.TransformatorFactory;

/**
 * Connector used for connection to a MART-Server.
 *
 * @author erbel
 *
 */
public class MartConnector extends AbsConnector {

	private String keyPath;
	private final String remoteRuntimeModelDir;
	private final String remoteRuntimeModelFile;

	/**
	 * Mart-Server constructor.
	 *
	 * @param address
	 *            Address to the MART Server.
	 * @param port
	 *            Port used by the MART Server.
	 * @param user
	 *            Username of the actual cloud.
	 * @param keyPath
	 *            Path to the SSH key to be used for deployment activities.
	 */
	public MartConnector(String address, int port, String user, String keyPath) {
		this.address = address;
		this.port = port;
		this.user = user;
		this.keyPath = keyPath;
		this.remoteRuntimeModelDir = "models";
		this.remoteRuntimeModelFile = "model-anonymous.occic";
		if (this.isReachable() == false) {
			throw new RuntimeException("Destination not reachable: " + this.address + ":" + this.port);
		}
	}

	public Path loadRuntimeModel(String rdirectory, String rfile, String lfile) {
		LOG.info("Loading Runtime Model: Location: " + rdirectory + "/" + rfile);
		ModelRetriever.retrieveRuntimeModel(rdirectory, rfile, lfile, this.address, this.port, this.user, this.keyPath);
		Resource res = ModelUtility.loadOCCIintoEMFResource(Paths.get(lfile));
		Transformator trans = TransformatorFactory.getTransformator("OCCI2OCCI");
		try {
			trans.transform(res, Paths.get(lfile));
		} catch (EolRuntimeException e) {
			LOG.fatal("An error occured when trying to transform the runtime model with the OCCI2OCCI Transformation."
					+ "Returning the original runtime model instead");
			return Paths.get(lfile);
		}
		// trans.transform(Paths.get(lfile), Paths.get(lfile));
		return Paths.get(lfile);
	}

	@Override
	public Path loadRuntimeModel(Path lfile) {
		return loadRuntimeModel(remoteRuntimeModelDir, remoteRuntimeModelFile, lfile.toString());
	}

}
