package de.ugoe.cs.rwm.docci.history;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.eclipse.cmf.occi.core.Entity;

import com.fasterxml.jackson.databind.ObjectMapper;

public class EntHistory extends AbsHistory {
	protected String entId;
	protected String entName;

	public EntHistory(Entity ent, long start, long end) {

		this.startDate = start;
		this.endDate = end;
		this.duration = calcDurationInMS(end, start);

		this.readableStartDate = new Date(this.startDate);
		this.readableEndDate = new Date(this.endDate);

		this.entId = ent.getId();
		if (ent.getTitle() == null) {
			this.entName = "null";
		} else {
			this.entName = ent.getTitle();
		}
	}

	public EntHistory() {

	}

	public void store(String jobHistoryPath) {
		if (jobHistoryPath != null && jobHistoryPath.equals("") == false) {
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				File logDir = new File(jobHistoryPath);
				if (!logDir.exists()) {
					if (logDir.mkdirs()) {
						System.out.println("Creating log Directory.");
					} else {
						System.out.println("Log Directory Could not be created.");
					}
				}
				objectMapper.writeValue(new File(jobHistoryPath + "/" + entId + ".json"), this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("No job history path set. Can not store task history!");
		}
	}

	public long getStartDate() {
		return startDate;
	}

	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}

	public long getEndDate() {
		return endDate;
	}

	public void setEndDate(long endDate) {
		this.endDate = endDate;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public Date getReadableStartDate() {
		Date d = readableStartDate;
		return d;
	}

	public void setReadableStartDate(Date readableStartDate) {
		this.readableStartDate = (Date) readableStartDate.clone();
	}

	public Date getReadableEndDate() {
		Date d = readableEndDate;
		return d;
	}

	public void setReadableEndDate(Date readableEndDate) {
		this.readableEndDate = (Date) readableEndDate.clone();
	}

	public String getEntId() {
		return entId;
	}

	public void setEntId(String entId) {
		this.entId = entId;
	}

	public String getEntName() {
		return entName;
	}

	public void setEntName(String entName) {
		this.entName = entName;
	}
}
