/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci.provisioner;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.eclipse.cmf.occi.core.Entity;
import org.eclipse.cmf.occi.core.Mixin;
import org.eclipse.cmf.occi.infrastructure.Compute;
import org.eclipse.cmf.occi.infrastructure.Network;
import org.eclipse.cmf.occi.infrastructure.Storage;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.JoinNode;

import de.ugoe.cs.rwm.docci.executor.Executor;
import de.ugoe.cs.rwm.docci.extractor.Extractor;
import de.ugoe.cs.rwm.docci.extractor.ExtractorFactory;

/**
 * Responsible for processing the workflow of an UML activity diagram and
 * perform corresponding actions.
 *
 * @author erbel
 *
 */
public class MartProvisioner extends AbsProvisioner implements Runnable {
	private static AtomicBoolean done = new AtomicBoolean();
	private final static Object LOCK = new Object();

	/**
	 * Creates Provisioner object with the ActivityNode being its current position.
	 *
	 * @param node
	 *            Current position of the provisioner.
	 */
	public MartProvisioner(ActivityNode node, Executor exec, EList<EObject> model) {
		this.currentNode = node;
		this.executor = exec;
		this.occiModel = model;
	}

	/**
	 * Performs provisionElements on next Node.
	 */
	void performInitial() {
		done.set(false);
		// done.setValue(false);
		this.provisionNextNode();

		synchronized (LOCK) {
			while (done.get() == false) {
				try {
					LOCK.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Start a thread for every outgoing control flow of the element.
	 */
	void performFork() {
		List<MartProvisioner> subtasks = new ArrayList<MartProvisioner>();
		subtasks.addAll(this.createSubtasks());
		LOG.debug("Threads created: " + subtasks.size());
		for (MartProvisioner subtask : subtasks) {
			Thread thread = new Thread(subtask);
			thread.start();
		}
	}

	/**
	 * Checks if all incoming edges of the node already arrived at the join. Only
	 * the last arriving control flow will continue to work on.
	 */
	void performJoin() {
		if (performed.containsAll(this.currentNode.getIncomings())) {
			performed.removeAll(this.currentNode.getIncomings());
			this.provisionNextNode();
		}
	}

	/**
	 * Serializes id swap list and deletes stubNW.
	 */
	void performFinal() {
		synchronized (LOCK) {
			// set ready flag to true (so isReady returns true)
			// done = true;
			done.set(true);
			LOCK.notifyAll();
		}
	}

	/**
	 * An extractor is created to search the topology model for the element with the
	 * id of the current node. Based upon that element an executor is created
	 * provisioning a resource based upon the information contained in the extracted
	 * element.
	 */
	void performAction() {
		try {
			Extractor extractor = ExtractorFactory.getExtractor("OCCI");
			EObject extractedEntity = extractor.extractElement(this.currentNode, this.occiModel);
			executor.executeOperation("PUT", extractedEntity, null);

			if (extractedEntity instanceof Compute || extractedEntity instanceof Network
					|| extractedEntity instanceof Storage) {
				boolean hasRuntime = false;
				for (Mixin mix : ((Entity) extractedEntity).getMixins()) {
					if (mix.getTerm().equals("runtimeid")) {
						hasRuntime = true;
					}
				}
				if (hasRuntime == false) {
					waitForActiveState(extractedEntity);
				}
			}
		} catch (Exception e) {
			LOG.error("Action could not be performed: " + this.currentNode);
			e.printStackTrace();
		} finally {
			performed.add(this.currentNode.getOutgoings().get(0));
			this.provisionNextNode();
		}
	}

	/**
	 * For every outgoing Edge of the currentNode create a new Provisioner. (Handle
	 * thread execution)
	 *
	 * @return List of required Provisioner for parallel execution.
	 */
	private List<MartProvisioner> createSubtasks() {
		List<MartProvisioner> subtasks = new ArrayList<MartProvisioner>();

		for (Iterator<ActivityEdge> iterator = (this.currentNode.getOutgoings().iterator()); iterator.hasNext();) {
			ActivityEdge edge = iterator.next();
			MartProvisioner subtask = new MartProvisioner(edge.getTarget(), this.executor, this.occiModel);
			subtask.setJobHistoryPath(jobHistoryPath);
			subtasks.add(subtask);
			if (edge.getTarget() instanceof JoinNode) {
				performed.add(edge);
			}
		}
		return subtasks;
	}
}
