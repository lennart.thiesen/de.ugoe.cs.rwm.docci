package de.ugoe.cs.rwm.docci.deprovisioner;

import org.eclipse.cmf.occi.core.Entity;

import de.ugoe.cs.rwm.docci.executor.Executor;
import de.ugoe.cs.rwm.docci.history.EntHistory;

public class DeprovisionerThread implements Runnable {
	private Entity ent;
	private String jhs;
	private Executor exec;

	public DeprovisionerThread(Entity ent, Executor exec, String jhs) {
		this.ent = ent;
		this.jhs = jhs;
		this.exec = exec;
	}

	@Override
	public void run() {
		long start = System.currentTimeMillis();
		deprovisionComputeInstance(ent);
		if (jhs != null) {
			EntHistory hist = new EntHistory(ent, start, System.currentTimeMillis());
			hist.store(jhs);
		}
	}

	/**
	 * Deprovisions a ComputeInstance
	 *
	 * @param entity Compute to be deprovisioned.
	 */
	private void deprovisionComputeInstance(Entity entity) {
		exec.executeOperation("DELETE", entity, null);
	}
}
