/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci.provisioner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.Entity;
import org.eclipse.cmf.occi.core.Resource;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.*;

import de.ugoe.cs.rwm.docci.executor.Executor;
import de.ugoe.cs.rwm.docci.extractor.Extractor;
import de.ugoe.cs.rwm.docci.extractor.ExtractorFactory;
import de.ugoe.cs.rwm.docci.history.EntHistory;

;

/**
 * Abstract Provisioner implementing the Provisioner Interface providing a
 * skeleton required by each provisioner.
 *
 * @author erbel
 *
 */
public abstract class AbsProvisioner implements Provisioner, Runnable {
	final static Logger LOG = Logger.getLogger(Provisioner.class.getName());
	protected Executor executor;
	protected EList<EObject> occiModel;
	protected ActivityNode currentNode;
	protected String jobHistoryPath;
	private static final int QUERYSLEEP = 2000;

	protected static volatile List<ActivityEdge> performed = Collections
			.synchronizedList(new ArrayList<ActivityEdge>());

	abstract void performInitial();

	abstract void performFork();

	abstract void performAction();

	abstract void performJoin();

	abstract void performFinal();

	/**
	 * Start of the provisioning process at the currentNode of the Provisioner.
	 * Depending on the currentElement specific operations are performed.
	 *
	 * InitialNode: Jump to the next node. Fork: Start threads to handle parallel
	 * control flows. OpaqueAction: Provision specific element Join: Wait for all
	 * incoming edges to finish. Starts a new thread handling the next node. Final:
	 * Notifies main waiting at initial to continue with the rest of the program.
	 */
	public void provisionElements() {
		if (this.currentNode.getName() != null) {
			LOG.info("Node Reached: " + this.currentNode.getName().toString());
		} else {
			LOG.info("Node Reached: " + this.currentNode.eClass().getName());
		}

		if (this.currentNode instanceof InitialNode) {
			this.performInitial();
		} else if (this.currentNode instanceof ForkNode) {
			this.performFork();
		} else if (this.currentNode instanceof OpaqueAction) {
			long start = System.currentTimeMillis();
			Extractor extractor = ExtractorFactory.getExtractor("OCCI");
			Entity entity = (Entity) extractor.extractElement(this.currentNode, this.occiModel);
			this.performAction();
			if (jobHistoryPath != null) {
				EntHistory hist = new EntHistory(entity, start, System.currentTimeMillis());
				hist.store(jobHistoryPath + "/prov");
			}
		} else if (this.currentNode instanceof JoinNode) {
			this.performJoin();
		} else if (this.currentNode instanceof ActivityFinalNode) {
			this.performFinal();
		}
	}

	/**
	 * Continues the control flow with the next element. Therefore, the currentNode
	 * is set to the following node and the method provisionElements is executed.
	 */
	protected void provisionNextNode() {
		this.currentNode = this.nextNode();
		this.provisionElements();
	}

	/**
	 * Returns the node the current one is connected to.
	 *
	 * @return next node.
	 */
	private ActivityNode nextNode() {
		return this.currentNode.getOutgoings().get(0).getTarget();
	}

	@Override
	public void run() {
		this.provisionElements();
	}

	/**
	 * Waits until EObject extracted is ready for further processing. For example,
	 * this function pauses one control flow until the Resource extracted is in
	 * state active.
	 *
	 * @param extracted Extracted OCCI element that needs to be in the active state.
	 */
	public void waitForActiveState(EObject extracted) {
		if (extracted instanceof Resource) {
			/*
			 * Due to the dependence of Container instances being connected to a Machine
			 * instance over Contains links it is not possible for a Container to reach the
			 * active state right after its creation.
			 */
			if (((Resource) extracted).getKind().getName().equals("Container")) {
				return;
			}
			Entity entity = (Entity) extracted;
			String output = executor.executeOperation("GET", entity, null);
			if (outputShowsActiveState(output)) {
				LOG.info("ACTIVE: " + ((Entity) extracted).getTitle());
			} else {
				try {
					LOG.debug("INACTIVE: " + ((Entity) extracted).getTitle() + " " + output);
					Thread.sleep(QUERYSLEEP);
					waitForActiveState(extracted);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Checks whether the output string contains an active or online.
	 *
	 * @param output Output to be checked.
	 * @return boolean Showing whether output contains an active attribute.
	 */
	private boolean outputShowsActiveState(String output) {
		output = output.replaceAll("\\s+", "");
		if (output.contains("occi.network.state=\"active\"") || output.contains("occi.compute.state=\"active\"")
				|| output.contains("occi.storage.state=\"online\"")
				|| output.contains("\"occi.network.state\":\"active\"")
				|| output.contains("\"occi.compute.state\":\"active\"")
				|| output.contains("\"occi.storage.state\":\"online\"")) {
			return true;
		}
		return false;
	}

	public void setJobHistoryPath(String jobHistoryPath) {
		this.jobHistoryPath = jobHistoryPath;
	}
}
