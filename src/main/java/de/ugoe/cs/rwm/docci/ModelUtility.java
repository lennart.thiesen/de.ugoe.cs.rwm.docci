/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci;

import java.io.File;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.*;
import org.eclipse.cmf.occi.core.util.OCCIResourceFactoryImpl;
import org.eclipse.cmf.occi.infrastructure.Compute;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.uml2.uml.InitialNode;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.internal.resource.UMLResourceFactoryImpl;
import org.modmacao.occi.platform.Application;
import org.modmacao.occi.platform.Component;
import org.modmacao.occi.platform.Componentlink;

import de.ugoe.cs.rwm.cocci.Comparator;
import de.ugoe.cs.rwm.cocci.ComparatorFactory;

/**
 * Responsible for EMF Model Utility Operations.
 *
 * @author erbel
 *
 */
public class ModelUtility {
	static Logger LOG = Logger.getLogger(Deployer.class.getName());

	/**
	 * Loads OCCI resource specified at the Path into a List of EObjects.
	 *
	 * @param occiPath OCCIModel to be loaded.
	 * @return List of EObjects representing the loaded OCCI model.
	 */
	public static EList<EObject> loadOCCI(Path occiPath) {
		Resource res = loadOCCIintoEMFResource(occiPath);
		if (getOCCIConfiguration(res) == null) {
			return getOCCIConfigurationContents(res);
		} else {
			return res.getContents();
		}
	}

	/**
	 * Returns Configuration contained within the specified OCCI model.
	 *
	 * @param occiPath Path to the OCCI model to be loaded.
	 * @return Configuration object describing the loaded OCCI model.
	 */
	public static Configuration loadOCCIConfiguration(Path occiPath) {
		Resource res = loadOCCIintoEMFResource(occiPath);
		if (getOCCIConfiguration(res) == null) {
			throw new NoSuchElementException("No Configuration found");
		}
		return getOCCIConfiguration(res);
	}

	/**
	 * Loads OCCI model into an EMFResource.
	 *
	 * @param configuration Path to the OCCI model to be loaded.
	 * @return EMFResource containing the loaded OCCI model.
	 */
	public static Resource loadOCCIintoEMFResource(Path configuration) {
		// InfrastructurePackage.eINSTANCE.eClass();
		// OCCIPackage.eINSTANCE.eClass();
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;

		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("occie", new OCCIResourceFactoryImpl());
		m.put("occic", new OCCIResourceFactoryImpl());

		ResourceSet resSet = new ResourceSetImpl();

		String file = new File(configuration.toString()).getAbsolutePath();
		Resource resource = resSet.getResource(URI.createFileURI(file), true);

		EcorePlugin.ExtensionProcessor.process(null);

		// Required for execution as standalone
		loadResources(resSet);
		addAttributesForStructuralFeatures(resource);

		// EcoreUtil.resolveAll(resSet);
		// Required for execution in eclipse

		return resource;
	}

	private static void addAttributesForStructuralFeatures(Resource resource) {
		addAttributesForKinds(resource);
		addAttributesForMixins(resource);
	}

	private static void addAttributesForMixins(Resource resource) {
		if (getOCCIConfigurationContents(resource) != null) {
			for (EObject obj : getOCCIConfigurationContents(resource)) {
				if (obj instanceof org.eclipse.cmf.occi.core.Resource) {
					org.eclipse.cmf.occi.core.Resource res = (org.eclipse.cmf.occi.core.Resource) obj;
					for (MixinBase mix : res.getParts()) {
						addMixinAttributesOfEntity(res, mix.eClass());
					}
					for (Link link : res.getLinks()) {
						for (MixinBase mixL : link.getParts()) {
							addMixinAttributesOfEntity(link, mixL.eClass());
						}
					}

				}
			}
		} else {
			for (EObject obj : getOCCIConfigurationContents(resource)) {
				if (obj instanceof org.eclipse.cmf.occi.core.Resource) {
					org.eclipse.cmf.occi.core.Resource res = (org.eclipse.cmf.occi.core.Resource) obj;
					for (Mixin mix : res.getMixins()) {
						addMixinAttributesOfEntity(res, mix.eClass());
					}
					for (Link link : res.getLinks()) {
						for (MixinBase mixL : link.getParts()) {
							addMixinAttributesOfEntity(link, mixL.eClass());
						}
					}
				}
			}
		}
	}

	private static void addMixinAttributesOfEntity(Entity ent, EClass eClass) {
		try {
			LOG.debug(("Adding attributes: " + ent.eClass().getName() + " : " + eClass.getName()));
			if (eClass.getEPackage().getNsURI().equals("http://schemas.ogf.org/occi/core/ecore") == false) {
				for (EStructuralFeature feat : eClass.getEAllStructuralFeatures()) {
					for (MixinBase part : ent.getParts()) {
						if (part.eClass().getName().equals(eClass.getName())) {
							if (part.eGet(feat) != null && feat.getEType() != null
									&& feat.getEType().getEPackage().getNsURI()
											.equals("http://schemas.ogf.org/occi/core/ecore") == false
									&& part.eIsSet(feat)) {
								if (part.getMixin() == null) {
									LOG.warn("Could not add Mixin Attr of Entity: " + ent.getTitle() + " ("
											+ ent.getKind().getTitle() + "); " + eClass.getName());
									return;
								}
								AttributeState attr = OCCIFactory.eINSTANCE.createAttributeState();
								attr.setName(createAttrName(feat.getName(), part.getMixin()));
								attr.setValue(part.eGet(feat).toString());

								AttributeState toSwitch = null;
								for (AttributeState registAttr : part.getAttributes()) {
									if (registAttr.getName().equals(attr.getName())) {
										toSwitch = registAttr;
									}
								}
								part.getAttributes().remove(toSwitch);

								AttributeState toSwitch2 = null;
								for (AttributeState registAttr : ent.getAttributes()) {
									if (registAttr.getName().equals(attr.getName())) {
										toSwitch2 = registAttr;
									}
								}
								part.getAttributes().remove(toSwitch2);

								part.getAttributes().add(attr);
							}
						}
					}

				}
			}
		} catch (RuntimeException e) {
			LOG.error("Mixin#Could not be loaded!");
			LOG.error("Entity: " + ent.getTitle() + " (" + ent.getKind().getTitle() + "); EClass: " + eClass.getName());
			e.printStackTrace();
		}

	}

	private static void addAttributesForKinds(Resource resource) {
		if (getOCCIConfigurationContents(resource) != null) {
			for (EObject obj : getOCCIConfigurationContents(resource)) {
				if (obj instanceof org.eclipse.cmf.occi.core.Resource) {
					org.eclipse.cmf.occi.core.Resource res = (org.eclipse.cmf.occi.core.Resource) obj;
					addAttributesOfEntity(res, res.eClass());
					for (Link link : res.getLinks()) {
						addAttributesOfEntity(link, link.eClass());
					}
				}
			}
		} else {
			for (EObject obj : getOCCIConfigurationContents(resource)) {
				if (obj instanceof org.eclipse.cmf.occi.core.Resource) {
					org.eclipse.cmf.occi.core.Resource res = (org.eclipse.cmf.occi.core.Resource) obj;
					addAttributesOfEntity(res, res.eClass());
					for (Link link : res.getLinks()) {
						addAttributesOfEntity(link, link.eClass());
					}
				}
			}
		}
	}

	private static void addAttributesOfEntity(Entity ent, EClass eClass) {
		try {
			LOG.debug(("Adding attributes: " + ent.eClass().getName() + " : " + eClass.getName()));
			if (eClass.getEPackage().getNsURI().equals("http://schemas.ogf.org/occi/core/ecore") == false) {
				for (EStructuralFeature feat : eClass.getEStructuralFeatures()) {
					if (ent.eGet(feat) != null) {
						if (ent.getKind() == null) {
							LOG.warn("Could not add Kind Attributes of Entity: " + ent.getTitle() + " ("
									+ ent.getKind().getTitle() + "); EClass: " + eClass.getName());
							return;
						}
						AttributeState attr = OCCIFactory.eINSTANCE.createAttributeState();
						attr.setName(createAttrName(feat.getName(), ent.getKind()));
						attr.setValue(ent.eGet(feat).toString());
						AttributeState toSwitch = null;
						for (AttributeState registAttr : ent.getAttributes()) {
							if (registAttr.getName().equals(attr.getName())) {
								toSwitch = registAttr;
							}
						}
						ent.getAttributes().remove(toSwitch);
						ent.getAttributes().add(attr);
					}
				}
				// TODO: consider supertypes
				if (eClass.getESuperTypes().size() == 1) {
					addAttributesOfEntity(ent, eClass.getESuperTypes().get(0));
				}
			}
		} catch (RuntimeException e) {
			LOG.error("Kind#Could not be loaded!");
		}
	}

	/*
	 * private static String createAttrName(String name) { String str =
	 * name.replaceAll("([A-Z])", ".$1").toLowerCase(); return str; }
	 */

	private static String createAttrName(String featureName, Kind kind) {
		String modifiedFeatureName = featureName.replaceAll("([A-Z])", ".$1").toLowerCase();
		for (Attribute as : kind.getAttributes()) {
			if (modifiedFeatureName.equals(as.getName())) {
				return modifiedFeatureName;
			}
		}
		for (Attribute as : getAllParentAttributes(kind)) {
			if (modifiedFeatureName.equals(as.getName())) {
				return modifiedFeatureName;
			}
		}
		return featureName;
	}

	private static List<Attribute> getAllParentAttributes(Kind kind) {
		List<Attribute> parentAttributes = new BasicEList<>();
		if (kind.getParent() == null) {
			return parentAttributes;
		} else {
			for (Attribute attr : kind.getParent().getAttributes()) {
				parentAttributes.add(attr);
			}
			parentAttributes.addAll(getAllParentAttributes(kind.getParent()));
		}
		return parentAttributes;
	}

	private static List<Attribute> getAllParentAttributes(Mixin mix) {
		List<Attribute> parentAttributes = new BasicEList<>();
		if (mix.getDepends() == null) {
			return parentAttributes;
		} else {
			for (Mixin parentMix : mix.getDepends()) {
				for (Attribute attr : parentMix.getAttributes()) {
					parentAttributes.add(attr);
				}
				parentAttributes.addAll(getAllParentAttributes(parentMix));
			}
		}
		return parentAttributes;
	}

	private static String createAttrName(String featureName, Mixin mix) {
		String modifiedFeatureName = featureName.replaceAll("([A-Z])", ".$1").toLowerCase();
		for (Attribute as : mix.getAttributes()) {
			if (modifiedFeatureName.equals(as.getName())) {
				return modifiedFeatureName;
			}
		}
		for (Attribute as : getAllParentAttributes(mix)) {
			if (modifiedFeatureName.equals(as.getName())) {
				return modifiedFeatureName;
			}
		}
		return modifiedFeatureName;
	}

	public static void loadResources(ResourceSet resSet) {
		for (Entry<URI, URI> entry : URIConverter.URI_MAP.entrySet()) {
			if (entry.getKey().toString().endsWith("occie")) {
				resSet.getResource(entry.getKey(), true);
			}
		}

		/*
		 * for (URI scheme : URIConverter.URI_MAP.keySet()) { if
		 * (URIConverter.URI_MAP.get(scheme).toString().endsWith("occie")) {
		 * resSet.getResource(scheme, true); } }
		 */
	}

	/**
	 * Simple comparison of the targetModel and runtimeModel based on the ID of each
	 * Entity. Examples uses: Test Cases, Check of Infrastructural layer before
	 * Application deployment.
	 *
	 * @param targetModel  First model.
	 * @param runtimeModel Second model.
	 * @return boolean describing whether both models are equal.
	 */
	public static boolean isEqual(Resource targetModel, Resource runtimeModel) {
		Comparator comp = ComparatorFactory.getComparator("Simple", runtimeModel, targetModel);
		if (comp.getMissingElements().isEmpty() == false || comp.getNewElements().isEmpty() == false) {
			LOG.info("Runtime Model does not conform to target model.");
			if (comp.getMissingElements().isEmpty() == false) {
				LOG.debug("Missing elements detected: " + comp.getMissingElements());
			}
			if (comp.getNewElements().isEmpty() == false) {
				LOG.debug("New elements detected: " + comp.getNewElements());
			}
			LOG.info("Skipping Application Deployment!");
			return false;
		}
		return true;
	}

	/**
	 * Returns contents of an OCCI configuration in form of an EObject list.
	 *
	 * @param resource Resource representing the OCCI configuration.
	 * @return List of EObject representing the OCCI configuration.
	 */
	public static EList<EObject> getOCCIConfigurationContents(Resource resource) {
		return getOCCIConfiguration(resource).eContents();
	}

	/**
	 * Returns OCCI Configuration contained within an EMFResource.
	 *
	 * @param resource EMFResource to be checked for an OCCI Configuration.
	 * @return OCCI Configuration contained within the passed EMFResource.
	 */
	public static Configuration getOCCIConfiguration(Resource resource) {
		for (EObject obj : resource.getContents()) {
			if (obj instanceof Configuration) {
				return (Configuration) obj;
			}
		}
		return null;
	}

	/**
	 * Loads an UML Model.
	 *
	 * @param path Path to the UML Model.
	 * @return UML Model Element.
	 */
	public static Model loadUML(Path path) {
		// Initialize the model
		UMLPackage.eINSTANCE.eClass();
		// Register the XMI resource factory for the .website extension
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("uml", new UMLResourceFactoryImpl());
		// Obtain a new resource set
		ResourceSet resSet = new ResourceSetImpl();
		// Get the resource
		Resource resource = resSet.getResource(URI.createFileURI(path.toString()), true);
		// Get the first model element and cast it to the right type, in my
		// example everything is hierarchical included in this first node
		Model model = (Model) resource.getContents().get(0);
		return model;
	}

	/**
	 * Returns first InitialNode of the Model.
	 *
	 * @param model Model containing activity diagram.
	 * @return InitialNode of the activity diagram.
	 */
	public InitialNode findInitialNode(Model model) {
		for (Iterator<EObject> iterator = model.eAllContents(); iterator.hasNext();) {
			EObject element = iterator.next();
			if (element instanceof InitialNode) {
				return (InitialNode) element;
			}
		}
		return null;
	}

	/**
	 * Returns all of the models top level and nested Entities as EObject List.
	 *
	 * @param model List of EObjects representing the OCCI model.
	 * @return EObject List containing all Entity elements contained in the model.
	 */
	public static EList<EObject> getEntities(EList<EObject> model) {
		EList<EObject> entities = new BasicEList<EObject>();
		entities.addAll(getTopLevelEntities(model));
		entities.addAll(getNestedEntities(model));
		return entities;
	}

	/**
	 * Returns all of the models top level and nested Resources as EObject List.
	 *
	 * @param model List of EObject representing the OCCI Model.
	 * @return EObject List containing all Entity elements contained in the model.
	 */
	public static EList<org.eclipse.cmf.occi.core.Resource> getResources(EList<EObject> model) {
		EList<EObject> entities = new BasicEList<EObject>();
		EList<org.eclipse.cmf.occi.core.Resource> resources = new BasicEList<org.eclipse.cmf.occi.core.Resource>();
		entities.addAll(getTopLevelEntities(model));
		entities.addAll(getNestedEntities(model));
		for (EObject entity : entities) {
			if (entity instanceof org.eclipse.cmf.occi.core.Resource) {
				// Guess that is not correct
				resources.add((org.eclipse.cmf.occi.core.Resource) entity);
			}
		}
		return resources;
	}

	/**
	 * Returns nested Entities in the Model.
	 *
	 * @param model List of EObjects representing the OCCI Model.
	 * @return List of nested Entities of the OCCI Model.
	 */
	private static EList<EObject> getNestedEntities(EList<EObject> model) {
		EList<EObject> entities = new BasicEList<EObject>();
		for (EObject topLevelElement : getTopLevelEntities(model)) {
			for (Iterator<EObject> iterator = ((Entity) topLevelElement).eAllContents(); iterator.hasNext();) {
				EObject nested = iterator.next();
				if (checkIfEntityElement(nested)) {
					entities.add(nested);
				}
			}
		}
		return entities;
	}

	/**
	 * Returns Top level Entities in the Model.
	 *
	 * @param model List of EObjects representing the OCCI model.
	 * @return Entities on the top level of the OCCI model.
	 */
	private static EList<EObject> getTopLevelEntities(EList<EObject> model) {
		EList<EObject> entities = new BasicEList<EObject>();
		for (EObject element : model) {
			if (checkIfEntityElement(element)) {
				entities.add(element);
			}
		}
		return entities;
	}

	/**
	 * Checks whether the element instantiates Entity.
	 *
	 * @param element Element to be checked
	 * @return boolean whether element is an Entity or not.
	 */
	public static boolean checkIfEntityElement(EObject element) {
		if (element instanceof org.eclipse.cmf.occi.core.Entity) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns path to the resource artifact.
	 *
	 * @param resourceName Name of the resource to be loaded
	 * @return String representing the path to the resource.
	 */
	public static String getPathToResource(String resourceName) {
		try {
			return ClassLoader.getSystemClassLoader().getResource(resourceName).getFile();
		} catch (NullPointerException e) {
			LOG.fatal("Resource " + resourceName + " could not be found in resource folder!");
		}
		return null;
	}

	/**
	 * Returns Action object of an action that can be performed on the passed
	 * Entity.
	 *
	 * @param ent        Entity on which the Action can be invoked.
	 * @param actionTerm Name of the Action that can be invoked.
	 * @return Actual Action element.
	 */
	public static Action getAction(Entity ent, String actionTerm) {
		return getActionByKind(ent.getKind(), actionTerm);
	}

	private static Action getActionByKind(Kind kind, String actionTerm) {
		for (Action act : kind.getActions()) {
			if (act.getTerm().equals(actionTerm)) {
				return act;
			}
		}
		if (kind.getParent() != null) {
			return getActionByKind(kind.getParent(), actionTerm);
		}
		throw new NoSuchElementException("Action not found in any related Kind!");
	}

	/**
	 * Returns List of Applications contained within the OCCI targetModel.
	 *
	 * @param targetModel EMFResource representing the OCCI model.
	 * @return List of Applications in form of OCCI Resources.
	 */
	public static EList<org.eclipse.cmf.occi.core.Resource> getApplications(Resource targetModel) {
		EList<org.eclipse.cmf.occi.core.Resource> apps = new BasicEList<org.eclipse.cmf.occi.core.Resource>();
		Configuration config = de.ugoe.cs.rwm.docci.ModelUtility.getOCCIConfiguration(targetModel);
		for (org.eclipse.cmf.occi.core.Resource res : config.getResources()) {
			if (res instanceof Application
					|| (res.getKind().getScheme().equals("http://schemas.modmacao.org/occi/platform#")
							&& res.getKind().getTerm().equals("application"))) {
				apps.add(res);
			}
		}
		return apps;
	}
	
	public static EList<org.eclipse.cmf.occi.core.Resource> getContainers(Resource targetModel) {
		EList<org.eclipse.cmf.occi.core.Resource> containers = new BasicEList<org.eclipse.cmf.occi.core.Resource>();
		Configuration config = de.ugoe.cs.rwm.docci.ModelUtility.getOCCIConfiguration(targetModel);
		for (org.eclipse.cmf.occi.core.Resource res : config.getResources()) {
			if (res.getKind().getScheme().equals("http://occiware.org/occi/docker#")
							&& res.getKind().getTerm().equals("container")) {
				containers.add(res);
			}
		}
		return containers;
	}

	/**
	 * Returns List of Componentlinks contained within the OCCI Resource.
	 *
	 * @param res OCCI Resource.
	 * @return List of Componenlinks in OCCI Resource.
	 */
	public static EList<org.eclipse.cmf.occi.core.Link> getComponenlinks(org.eclipse.cmf.occi.core.Resource res) {
		EList<org.eclipse.cmf.occi.core.Link> cLinks = new BasicEList<org.eclipse.cmf.occi.core.Link>();
		for (org.eclipse.cmf.occi.core.Link link : res.getLinks()) {
			if (res instanceof Componentlink
					|| (res.getKind().getScheme().equals("http://schemas.modmacao.org/occi/platform#")
							&& res.getKind().getTerm().equals("componentlink"))) {
				cLinks.add(link);
			}
		}
		return cLinks;
	}

	/**
	 * Checks whether res is a Component based on instance or scheme.
	 *
	 * @param res OCCI Resource.
	 * @return Boolean whether Resource is Component.
	 */
	public static Boolean isComponent(org.eclipse.cmf.occi.core.Resource res) {
		if (res instanceof Component || (res.getKind().getScheme().equals("http://schemas.modmacao.org/occi/platform#")
				&& res.getKind().getTerm().equals("component"))) {
			return true;
		}

		return false;
	}

	/**
	 * Checks whether res is a Compute based on instance or scheme.
	 *
	 * @param res OCCI Resource.
	 * @return Boolean whether Resource is Compute.
	 */
	public static boolean isCompute(org.eclipse.cmf.occi.core.Resource res) {
		if (res instanceof Compute || (res.getKind().getScheme().equals("http://schemas.ogf.org/occi/infrastructure#")
				&& res.getKind().getTerm().equals("compute"))) {
			return true;
		}
		return false;
	}
	
	public static boolean isMachine(org.eclipse.cmf.occi.core.Resource res) {
		if (res.getKind().getScheme().equals("http://occiware.org/occi/docker#")
				&& res.getKind().getTerm().equals("machine")) {
			return true;
		}
		return false;
	}
}
