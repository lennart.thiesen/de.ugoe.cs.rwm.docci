package de.ugoe.cs.rwm.docci.containerrunner;

import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.Action;
import org.eclipse.cmf.occi.core.Resource;

import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.executor.Executor;
import de.ugoe.cs.rwm.docci.executor.ExecutorFactory;
import de.ugoe.cs.rwm.docci.history.DeplHistory;

public class MartContainerRunnerSlave implements Runnable {
	static Logger LOGGER = Logger.getLogger(MartContainerRunnerSlave.class);
	private Resource container;
	private Connector conn;
	private String jhs;

	public MartContainerRunnerSlave(Connector conn, Resource container, String jhs) {
		this.conn = conn;
		this.container = container;
		this.jhs = jhs;
	}

	@Override
	public void run() {
		this.runContainer();
	}

	private void runContainer() {
		long deplStart = System.currentTimeMillis();
		Executor exec = ExecutorFactory.getExecutor("Mart", conn);
		LOGGER.info("Deploying: " + container.getTitle());

		Action run = ModelUtility.getAction(container, "run");
		exec.executeOperation("POST", container, run);
		if (jhs != null) {
			DeplHistory hist = new DeplHistory(container, deplStart, deplStart, deplStart, deplStart, deplStart,
					System.currentTimeMillis());
			hist.store(jhs + "/depl");
		}
	}
}
