# DOCCI

[![pipeline status](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.docci/badges/master/pipeline.svg)](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.docci/commits/master)
[![coverage report](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.docci/badges/master/coverage.svg)](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.docci/commits/master)

Docci is a models at runtime engine that adapts running cloud deployments using the Open Cloud Computing Interface (OCCI).
Docci extracts a runtime model from the OCCI API compares it to the desired cloud deployment and derives required REST requests.

## Requirements
To use this engine a OCCI API is required. Currently only the MartServer is supported.
Please refer to the documentations of the [MartServer](https://github.com/occiware/MartServer).

Note: An [example MartServer](src/test/resources) is used to test this engine in our pipeline which can be started as a standalone Jar.

## Getting Started
Currently, no graphical or commandline interface is provided for this engine.
Thus, it can only be used as a library which can be downloaded over a [Nexus repository](https://nexus.informatik.uni-goettingen.de/content/repositories/rwm/).

### Examples
To see how the engine can be used, checkout the project and run it as gradle tests.

Warning: The test cases automatically start a MartServer and place its required folders such as its plugins and model folder in your home directory.
Therfore, it is recommended to start the server in form of a [docker container](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mocci/blob/master/doc/api.md).
Moreover, the live tests have to be adjusted as the MartServer is not located at localhost anymore.
To execute the live tests they have to be included in the [gradle build](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.docci/blob/master/build.gradle) (comment out: exlude/live).

### Configuring the MartServer to be used in OpenStack
[Documentation on how to setup and configure the MartServer for an OpenStack Cloud](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mocci/blob/master/doc/api.md)

